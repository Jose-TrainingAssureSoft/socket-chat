const { io } = require('./server');
const User = require('../classes/user');
const { crearMensaje } = require('../classes/message');

const users = new User();

io.on('connection', (client) => {

    client.on('entrarChat', (data, callback) => {
        //Uniendo a una sala en particular
        client.join(data.sala);

        users.agregarPersona(client.id, data.nombre, data.sala);
        callback(`# ${data.sala}`);
    });

    client.on('crearMensaje', (data, callback) => {

        if(data){
            let mensaje = crearMensaje(data.nombre, data.mensaje);
            client.broadcast.to('sala').emit('crearMensaje', mensaje);
            callback(mensaje);
        }
    });

    client.on('disconnect', () => {
        console.log(`${client.id} salió`);
    });
});