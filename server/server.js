require('dotenv').config();
const express = require('express');
const app = express();
const server = require('http').Server(app);
const path = require('path');
const io = require('socket.io')(server);

const publicPath = path.resolve(__dirname, '../public');

// IO = This is the socket backend comunication
module.exports.io = io;
require('./sockets');

//Connection to json
app.use(express.json());

server.listen(process.env.PORT, (err) => {
    if (err) throw new Error(err);
    console.log('Conectado al puerto', process.env.PORT);
});

app.use(express.static(publicPath));