var socket = io();

let usuario = {
    nombre:'jose', // params.get('nombre'),
    sala: 'sala' //params.get('sala')
};

socket.on('connect', function() {
    console.log('Socket connected');

    socket.emit('entrarChat', usuario, function(resp) {
        console.log(resp);
    });
  
    //Enviar información
    socket.on('crearMensaje', function(data) {
       otherMessage({mensaje:data.mensaje});
    });
});

// escuchar
socket.on('disconnect', function() {
    console.log('Perdimos conexión con el servidor');
});
    