var socket = io();

let message = $('#messages');
let sendButton = $('#sendMessage');

function otherMessage(mensaje) {
    let html = '';
    html += '<li class="other">';
    html +=  mensaje.mensaje;
    html += '</li>';
    message.append(html);
}

function selfMessage(mensaje) {
    let html = '';
    html += '<li class="self">';
    html +=  mensaje.mensaje;
    html += '</li>';
    message.append(html);
}
    
sendButton.on('click', function() {
    let userInput = $('.text-box');
    let newMessage = userInput.html().replace(/\<div\>|\<br.*?\>/ig, '\n').replace(/\<\/div\>/g, '').trim().replace(/\n/g, '<br>');
    
    socket.emit('crearMensaje', {
        nombre: 'Fernando',
        mensaje: newMessage
    }, function(resp) {
        // otherMessage({mensaje:resp.mensaje});
    });
});